#pragma once

#include <cstring>

#include <string>

#include <unistd.h>

namespace snow {

inline std::string get_user_name() {
    constexpr std::size_t USER_NAME_LENGTH = 80;
    char user_name[USER_NAME_LENGTH];
    std::memset(user_name, 0, USER_NAME_LENGTH);
    return ::getlogin_r(user_name, USER_NAME_LENGTH) == 0 ? user_name : "";
}

inline std::string get_host_name() {
    constexpr std::size_t HOST_NAME_LENGTH = 80;
    char host_name[HOST_NAME_LENGTH];
    std::memset(host_name, 0, HOST_NAME_LENGTH);
    return ::gethostname(host_name, HOST_NAME_LENGTH) == 0 ? host_name : "";
}

} // namespace snow
