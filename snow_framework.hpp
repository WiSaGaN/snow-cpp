#include <snow_config.hpp>
#include <snow.hpp>

#include <spdlog/spdlog.h>

namespace snow {

std::shared_ptr<spdlog::logger> init_logger(std::size_t log_message_queue_size, std::size_t log_flush_interval_milliseconds, std::string const &service_name) {
    spdlog::set_pattern("%Y-%m-%d %T.%f [%t][%l] %v");
    spdlog::set_async_mode(log_message_queue_size,
            spdlog::async_overflow_policy::block_retry,
            nullptr,
            std::chrono::milliseconds(log_flush_interval_milliseconds));
    auto logger = spdlog::daily_logger_st("spdlog", service_name, 0, 0);
    logger->info() << "================================================================================";
    logger->info() << "Service: " << service_name;
    logger->info() << "User " << snow::get_user_name() << " running on " << snow::get_host_name();
    logger->info() << "Binary name: " << snow::get_project_name();
    logger->info() << "Binary version: " << snow::get_project_version();
    if (snow::get_binary_is_snapshot()) {
        logger->warn() << "Binary is SNAPSHOT.";
    }
    logger->info() << "Binary git short hash: " << snow::get_git_short_hash();
    if (snow::get_git_source_is_dirty()) {
        logger->warn() << "Binary git source is dirty.";
    }
    logger->info() << "Binary cmake build type: " << snow::get_cmake_build_type();
    logger->info() << "Binary built on " << snow::get_build_os();
    logger->info() << "Binary built at " << snow::get_build_time();
    return logger;
}

} // namespace snow
