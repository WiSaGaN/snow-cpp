#snow
snow is a C++ project framework that provides basic compile-time/run-time constants, and build tools.

##Dependency
Main project must use git and CMake, and have a C++11 compatible compiler. Snow dictates that project also needs to use cpptoml as config file parser, and spdlog as async logger.

##Usage
0. Start a new project. Use `git init` to initialize the project.
1. `git submodule add git@bitbucket.org:WiSaGaN/snow.git`
2. `./snow/setup_basic` setup the basic structure for a snow project.
3. `./snow/build` builds this base setup.
4. Commit the project to master, and checkout new develop branch. Start development on branch develop.
5. Use snow/build script to build and package the project.
6. When ready, make project git clean, and use snow/release to auto remove "-SNAPSHOT" and release it. The release script will use "a successful git work flow" to merge the release version to master, and tag it.
7. Use snow/bump to bump to version and start next round of development.
