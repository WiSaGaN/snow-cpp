#include <snow_framework.hpp>

#include <iostream>

constexpr std::size_t LOG_MESSAGE_QUEUE_SIZE = 256;
constexpr std::size_t LOG_FLUSH_INTERVAL_MILLISECONDS = 500;

int main(int argc, char *argv[]) {
    auto logger = snow::init_logger(LOG_MESSAGE_QUEUE_SIZE, LOG_FLUSH_INTERVAL_MILLISECONDS, "hello-world");
    std::cout << "Hello world." << std::endl;
    logger->info() << "Hello world.";
    return 0;
}
